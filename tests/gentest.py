"""
Regression testing for the intelurls code.

As you can see, there are a -lot- of combinations that need to be tested.
"""
import json
import sys
import os

import intelurls

test_urls = [
# Apple Maps - iOS 8.4.1 - share/copy/paste
"http://maps.apple.com/?lsp=6489&sll=37.758460,-122.415427&q=619%20Shotwell%20St&hnear=619%20Shotwell%20St%2C%20San%20Francisco%2C%20CA%20%2094110%2C%20United%20States",

# Google Maps - Android 5 "search" version
"https://www.google.com/maps/search/45.025257,14.627524/data=!4m2!2m1!4b1?hl=en&dg=dbrw&newdg=1",

# Google Maps - shared to Hangouts from Android 5
"""Sundance Cinemas

http://goo.gl/maps/VU4ym https://plus.google.com/104443216790055508247/about?hl=en""",

# Modified to support worst case with URL in the middle of a string (hopefully never happens)
"Sundance Cinemas http://goo.gl/maps/CKxf8 https://plus.google.com/104443216790055508247/about?hl=en",

# iOS with caption w/apostrophe
"https://maps.google.com/maps?ll=37.765727,-122.431961&cid=10889150637731333995&q=Beck's%20Motor%20Lodge",

# iOS without caption
"https://maps.google.com/maps?ll=37.765994,-122.432624&q=37.765994,-122.432624",

# Android HO 4.0
"https://maps.google.com/maps?q=37.765122,-122.431277",

# Some unknown map share link (may not be real)
"http://maps.google.com/?q=44.949711,14.462425&hl=en&gl=us",

# Ingress Intel
"https://intel.ingress.com/intel?ll=37.821523,-122.377385&z=17",
"https://intel.ingress.com/intel?pll=37.821523,-122.377385&z=17",
"https://intel.ingress.com/intel?ll=37.821523,-122.377385&z=17&pll=37.821523,-122.377385",
# Unknown
"https://google.com/maps?q=37.765122,-122.431277",

# iOS with caption with Ampersand
"https://www.google.com/maps?ll=37.764842,-122.431898&cid=5053626698704461875&q=Peet%27s+Coffee+&+Tea",

# Shortened Google Map
"http://goo.gl/maps/Zqqb2",

# Another...
"http://goo.gl/maps/432Ww",

# and another
"http://goo.gl/maps/r6T6a",

# ...which expanded to:
"https://www.google.com/maps/place/Vol%C4%8Dji+Potok+3a,+1235+Radomlje/data=!4m2!3m1!1s0x4765358e663abadf:0x7017dcda30e59063?hl=en&dg=dbrw&newdg=1",

# ...which then got magically rewritten by javascript to:
"https://www.google.com/maps/place/Vol%C4%8Dji+Potok+3a,+1235+Radomlje,+Slovenia/@46.1879318,14.6130029,17z/data=!3m1!4b1!4m2!3m1!1s0x4765358e663abadf:0x7017dcda30e59063?hl=en",

# Google Maps w/Places
"https://www.google.com/maps/place/45%C2%B001'31.1%22N+14%C2%B037'39.0%22E/@45.0252969,14.6274859,17z/data=!3m1!4b1!4m2!3m1!1s0x0:0x0",

# new iOS hangouts
"https://maps.google.com/maps?q=Twin%20Peaks,%20San%20Francisco,%20San%20Francisco%20County,%20California@37.754407,-122.447684"
]

test_locations = [
    "San Francisco, CA",
    "1 Market Street, San Francisco, CA",
    "Klortfndub, Xoinajhd"      # invalid location
]

def main():
    cases = {}

    cases["is_map"]  = []
    for url in test_urls:
        cases["is_map"].append([url, intelurls.check_mapurl(url)])

    cases["parse_map"] = []
    for url in test_urls:
        cases["parse_map"].append([url, intelurls.parse_mapurl(url)])

    cases["parse_location"] = []
    for location in test_locations:
        cases["parse_location"].append([location, intelurls.parse_natural(location)])

    print(json.dumps(cases, sort_keys=True, indent=2))

if __name__ == '__main__':
    main()
